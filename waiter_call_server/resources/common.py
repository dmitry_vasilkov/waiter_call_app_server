import jwt
from flask_jwt_extended import get_jwt_claims, get_jwt_identity
from flask_jwt_extended.exceptions import JWTExtendedException
from flask import request


from run import JWT_ROLE_WAITER, JWT_ROLE_CLIENT, app, SERVICE_USERNAME

MIN_ALLOWED_CODE = 0
MAX_ALLOWED_CODE = 9


def log_exception_with_request(text=""):
    request_string = "{} {} {} {}".format(request.url, request.method, request.data, request.headers.get('Authorization'))
    app.logger.exception("{} Exception in request {}".format(text, request_string))


def user_is_client():
    return get_jwt_claims()['role'] == JWT_ROLE_CLIENT


def user_is_waiter():
    return get_jwt_claims()['role'] == JWT_ROLE_WAITER


def user_is_admin():
    return get_jwt_claims()['role'] == JWT_ROLE_WAITER and get_jwt_identity() == SERVICE_USERNAME


def api_for_waiter(func):
    """
    Use this decorator if you want to make this api only for waiters
    """
    def func_wrapper(*args, **kwargs):
        if not user_is_waiter():
            return {'message': 'You cant use this API method'}, 403
        return func(*args, **kwargs)
    return func_wrapper


def api_for_client(func):
    """
    Use this decorator if you want to make this api only for clients
    """
    def func_wrapper(*args, **kwargs):
        if not user_is_client():
            return {'message': 'You cant use this API method'}, 403
        return func(*args, **kwargs)
    return func_wrapper


def api_for_admin(func):
    """
    Use this decorator if you want to make this api only for admins
    """
    def func_wrapper(*args, **kwargs):
        if not user_is_admin():
            return {'message': 'You cant use this API method'}, 403
        return func(*args, **kwargs)
    return func_wrapper


def handle_auth_errors(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (jwt.exceptions.InvalidTokenError, JWTExtendedException)as e:
            log_exception_with_request()
            return {'message': "Incorrect access token"}, 401
    return func_wrapper

