import datetime
from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, get_jwt_identity, get_raw_jwt, create_access_token


from models import MessageModel, SessionModel
from resources.common import api_for_client, handle_auth_errors, log_exception_with_request
from run import JWTUserObject, JWT_ROLE_CLIENT, limiter


class RegisterCode(Resource):
    # commented for debug
    # decorators = [limiter.limit("50 per day"), limiter.limit("10 per hour"), limiter.limit("5 per minute")]

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('client_code', help='You must provide this field(int)', required=True, type=int)
        data = parser.parse_args()
        try:
            client_code = data['client_code']
            for session in SessionModel.get_active_sessions():
                if session.client_code == client_code:
                    if not session.is_registered:
                        session.is_registered = True
                        session.registration_date = datetime.datetime.utcnow()
                        session.safe_commit()
                        user = JWTUserObject(username=client_code, role=JWT_ROLE_CLIENT)
                        access_token = create_access_token(identity=user)
                        return {
                            'message': 'Code successfully registered',
                            'data': {'access_token': access_token}
                        }
                    else:
                        return {'message': 'Code already registered'}, 500
            return {'message': 'Incorrect client code'}, 500

        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


class CallWaiter(Resource):
    @handle_auth_errors
    @jwt_required
    @api_for_client
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('message_content', help='You must provide this field(string)', required=True)
        data = parser.parse_args()
        try:
            message_content = data['message_content']
            client_code = get_jwt_identity()
            for session in SessionModel.get_active_sessions():
                if session.client_code == client_code and session.is_registered is True:
                    token_issue_date = datetime.datetime.fromtimestamp(get_raw_jwt()['iat'])
                    if token_issue_date < session.creation_date:
                        return {'message': 'You cant use this token'}, 403

                    message = MessageModel(message_content=message_content,
                                           user=session.user,
                                           client_code=client_code,
                                           is_sent=False,
                                           creation_date=datetime.datetime.utcnow(),
                                           table_number=session.table_number
                                           )
                    message.add_to_db()
                    return {'message': 'Successfully sent'}

            return {'message': 'Server error, cant find active session with this token'}, 500
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


class EndSession(Resource):
    @handle_auth_errors
    @jwt_required
    @api_for_client
    def post(self):
        try:
            client_code = get_jwt_identity()
            for session in SessionModel.get_active_sessions():
                if session.client_code == client_code and session.is_registered is True:
                    token_issue_date = datetime.datetime.fromtimestamp(get_raw_jwt()['iat'])
                    if token_issue_date < session.creation_date:
                        return {'message': 'You cant use this token'}, 500

                    session.end_date = datetime.datetime.utcnow()
                    session.is_active = False
                    session.safe_commit()
                    return {'message': 'Session finished successfully'}

            return {'message': 'Server error, cant find active session with this token'}, 500
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500
