import random
import datetime
import time
from flask_restful import Resource, reqparse
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, get_raw_jwt

from models import UserModel, RevokedTokenModel, MessageModel, SessionModel
from run import JWTUserObject, JWT_ROLE_WAITER, limiter
from resources.common import handle_auth_errors,  MIN_ALLOWED_CODE, MAX_ALLOWED_CODE, api_for_waiter, log_exception_with_request


class Login(Resource):
    # commented for debug
    # decorators = [limiter.limit("50 per day"), limiter.limit("10 per hour"), limiter.limit("5 per minute")]

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('username', help='You must provide this field(string)', required=True)
        parser.add_argument('password', help='You must provide this field(string)', required=True)
        data = parser.parse_args()
        current_user = UserModel.find_by_username(data['username'])
        if not current_user:
            return {'message': 'User {} doesn\'t exist'.format(data['username'])}, 500

        if not UserModel.verify_hash(data['password'], current_user.password_hash):
            return {'message': 'Wrong credentials'}, 401

        if current_user.restaurant.is_license_active is not True:
            return {'message': 'Sorry, your license is not active'}, 403

        user = JWTUserObject(username=data['username'], role=JWT_ROLE_WAITER)
        access_token = create_access_token(identity=user)
        return {
            'message': 'Logged in as {}'.format(current_user.username),
            'data': {'access_token': access_token}
            }


class Logout(Resource):
    @handle_auth_errors
    @jwt_required
    @api_for_waiter
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Access token has been deleted'}
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


# class UserLogoutRefresh(Resource):
#     def post(self):
#         return {'message': 'User logout'}


# class TokenRefresh(Resource):
#     @jwt_refresh_token_required
#     def post(self):
#         current_user = get_jwt_identity()
#         access_token = create_access_token(identity=current_user)
#         return {'access_token': access_token}


class GenerateClientCode(Resource):
    """
    Add to sessions table new session with randomly generated code.
    """
    @handle_auth_errors
    @jwt_required
    @api_for_waiter
    def post(self):
        user = UserModel.find_by_username(get_jwt_identity())
        if user is None:
            return {'message': 'Unknown user'}, 401

        if user.restaurant.is_license_active is not True:
            return {'message': 'Sorry, your license is not active'}, 403

        parser = reqparse.RequestParser()
        parser.add_argument('table_number', help='You must provide this field(int)', required=True, type=int)
        data = parser.parse_args()
        try:
            table_number = data['table_number']
            used_codes = set(map(lambda x: x.client_code, SessionModel.get_active_sessions()))
            all_codes = set(i for i in range(MIN_ALLOWED_CODE, MAX_ALLOWED_CODE + 1))
            free_codes = all_codes - used_codes
            if len(free_codes) == 0:
                return {'message': 'No free code could be found, try later'}, 500
            client_code = list(free_codes)[random.randint(0, len(free_codes) - 1)]
            new_session = SessionModel(
                user=user,
                table_number=table_number,
                client_code=client_code,
                creation_date=datetime.datetime.utcnow(),
                is_active=True
            )
            new_session.add_to_db()
            return {
                'message': 'Code successfully generated',
                'data': {'client_code': client_code}
            }
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


class EndClientSession(Resource):
    """
    Mark active session with specified client_code unactive(is_active=false)
    """
    @handle_auth_errors
    @jwt_required
    @api_for_waiter
    def post(self):
        user = UserModel.find_by_username(get_jwt_identity())
        if user is None:
            return {'message': 'Unknown user'}, 401

        parser = reqparse.RequestParser()
        parser.add_argument('client_code', help='You must provide this field(int)', required=True, type=int)
        data = parser.parse_args()
        try:
            client_code = data['client_code']
            is_code_freed = False
            for session in SessionModel.get_active_sessions():
                if session.client_code == client_code:
                    if session.user.restaurant != user.restaurant:
                        return {'message': 'Sorry, you cant free sessions in another restaurant'}, 403
                    session.end_date = datetime.datetime.utcnow()
                    session.is_active = False
                    session.safe_commit()
                    is_code_freed = True
                    break
            if is_code_freed:
                return {'message': 'Success'}
            else:
                return {'message': 'Unknown code'}, 500
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


class GetActiveRegisteredSessions(Resource):
    """
    Get list of sessions with registered codes for current restaurant
    """
    @handle_auth_errors
    @jwt_required
    @api_for_waiter
    def get(self):
        user = UserModel.find_by_username(get_jwt_identity())
        if user is None:
            return {'message': 'Unknown user'}, 401

        try:
            sessions_for_current_restaurant = []
            for session in SessionModel.get_active_sessions():
                print(session)
                if session.is_active is True and session.is_registered is True and session.user.restaurant == user.restaurant:
                    sessions_for_current_restaurant.append({
                        'user_id': session.user_id,
                        'creation_date': time.mktime(session.creation_date.timetuple()) if session.creation_date else None,
                        'registration_date': time.mktime(session.registration_date.timetuple()) if session.registration_date else None,
                        'last_active_date': time.mktime(session.last_active_date.timetuple()) if session.last_active_date else None,
                        'table_number': session.table_number,
                        'client_code': session.client_code
                    })
            return {'message': 'success', 'data': {'sessions': sessions_for_current_restaurant}}
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


class GetActiveSessions(Resource):
    """
    Get list of active sessions for current restaurant
    """
    @handle_auth_errors
    @jwt_required
    @api_for_waiter
    def get(self):
        user = UserModel.find_by_username(get_jwt_identity())
        if user is None:
            return {'message': 'Unknown user'}, 401

        try:
            sessions_for_current_restaurant = []
            for session in SessionModel.get_active_sessions():
                print(session)
                if session.is_active is True and session.user.restaurant == user.restaurant:
                    sessions_for_current_restaurant.append({
                        'user_id': session.user_id,
                        'creation_date': time.mktime(session.creation_date.timetuple()) if session.creation_date else None,
                        'registration_date': time.mktime(session.registration_date.timetuple()) if session.registration_date else None,
                        'last_active_date': time.mktime(session.last_active_date.timetuple()) if session.last_active_date else None,
                        'table_number': session.table_number,
                        'client_code': session.client_code,
                        'is_registered': session.is_registered
                    })
            return {'message': 'success', 'data': {'sessions': sessions_for_current_restaurant}}
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


class GetNewMessages(Resource):
    """
    Get unsent messages for current waiter, mark them as sent.
    """
    @handle_auth_errors
    @jwt_required
    @api_for_waiter
    def get(self):
        try:
            user = UserModel.find_by_username(get_jwt_identity())
            if user is None:
                return {'message': 'Unknown user'}, 401
            new_messages = MessageModel.get_new_messages_for_user(user)
            new_messages_list = []
            for new_message in new_messages:
                new_messages_list.append({
                    'message_content': new_message.message_content,
                    'table_number': new_message.table_number,
                    'creation_date': new_message.creation_date,
                    'client_code': new_message.client_code
                })
                new_message.is_sent = True
                new_message.safe_commit()

            return {
                'message': 'Success',
                'data': {'new_messages': new_messages_list}
            }
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500
