from flask_restful import Resource, reqparse
from flask_jwt_extended import create_access_token, jwt_required


from models import UserModel, RestaurantModel
from run import JWTUserObject, JWT_ROLE_WAITER
from resources.common import log_exception_with_request, handle_auth_errors, api_for_admin


class UserRegistration(Resource):
    @handle_auth_errors
    @jwt_required
    @api_for_admin
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('username', help='You must provide this field(string)', required=True)
        parser.add_argument('password', help='You must provide this field(string)', required=True)
        parser.add_argument('restaurant_id', help='You must provide this field(int)', required=True, type=int)
        data = parser.parse_args()

        if UserModel.find_by_username(data['username']):
            return {'message': 'User {} already exists'.format(data['username'])}, 500

        new_user = UserModel(
            username=data['username'],
            password_hash=UserModel.generate_hash(data['password']),
            restaurant_id=data['restaurant_id']
        )
        try:
            new_user.add_to_db()
            user = JWTUserObject(username=data['username'], role=JWT_ROLE_WAITER)
            access_token = create_access_token(identity=user)
            return {
                'message': 'User {} created successfully'.format(data['username']),
                'data': {'access_token': access_token}
                }
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


class RestaurantRegistration(Resource):
    @handle_auth_errors
    @jwt_required
    @api_for_admin
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('name', help='You must provide this field(string)', required=True)
        parser.add_argument('is_license_active', help='You must provide this field(bool)', required=True, type=bool)
        data = parser.parse_args()

        if RestaurantModel.find_by_name(data['name']):
            return {'message': 'Restaurant {} already exists'.format(data['name'])}, 500

        new_restaurant = RestaurantModel(
            name=data['name'],
            is_license_active=data['is_license_active']
        )
        try:
            new_restaurant.add_to_db()
            return {
                'message': 'Restaurant {} created'.format(data['name']),
                'data': {'restaurant_id': new_restaurant.id}
                }
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


class AllUsers(Resource):
    @handle_auth_errors
    @jwt_required
    @api_for_admin
    def get(self):
        return UserModel.return_all()

    @handle_auth_errors
    @jwt_required
    @api_for_admin
    def delete(self):
        return UserModel.delete_all()


class AllRestaurants(Resource):
    @handle_auth_errors
    @jwt_required
    @api_for_admin
    def get(self):
        return RestaurantModel.return_all()

    @handle_auth_errors
    @jwt_required
    @api_for_admin
    def delete(self):
        return RestaurantModel.delete_all()


class User(Resource):
    @handle_auth_errors
    @jwt_required
    @api_for_admin
    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument('username', help='You must provide this field(string)', required=True)
        data = parser.parse_args()
        try:
            user = UserModel.find_by_username(username=data['username'])
            if user is None:
                return {'message': 'No such user'}, 500
            user.delete_from_db()
            return {'message': 'Success'}
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


class Restaurant(Resource):
    @handle_auth_errors
    @jwt_required
    @api_for_admin
    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', help='You must provide this field(string)', required=True)
        data = parser.parse_args()
        try:
            restaurant = RestaurantModel.find_by_name(name=data['username'])
            if restaurant is None:
                return {'message': 'No such restaurant'}, 500
            restaurant.delete_from_db()
            return {'message': 'Success'}
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500


class RestaurantLicense(Resource):
    @handle_auth_errors
    @jwt_required
    @api_for_admin
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', help='You must provide this field(string)', required=True)
        parser.add_argument('is_license_active', help='You must provide this field(bool)', required=True, type=bool)
        data = parser.parse_args()
        try:
            restaurant = RestaurantModel.find_by_name(name=data['name'])
            if restaurant is None:
                return {'message': 'No such restaurant'}, 500
            restaurant.is_license_active = data['is_license_active']
            restaurant.safe_commit()
            return {'message': 'Successfully set is_license_active to {}'.format(data['is_license_active'])}
        except:
            log_exception_with_request()
            return {'message': 'Something went wrong'}, 500

