import logging
from logging.handlers import TimedRotatingFileHandler
from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

app = Flask(__name__)
api = Api(app)
limiter = Limiter(
    app,
    key_func=get_remote_address
)


def register_service_models():
    restaurants = models.RestaurantModel.get_all()
    service_restaurant_found = False
    for restaurant in restaurants:
        if restaurant.name == SERVICE_RESTAURANT_NAME:
            service_restaurant_found = True
            break
    if not service_restaurant_found:
        new_restaurant = models.RestaurantModel(
            name=SERVICE_RESTAURANT_NAME,
            is_license_active=True
        )
        new_restaurant.add_to_db()

    service_restaurant_id = models.RestaurantModel.find_by_name(name=SERVICE_RESTAURANT_NAME).id

    users = models.UserModel.get_all()
    service_user_found = False
    for user in users:
        if user.username == SERVICE_USERNAME:
            service_user_found = True
            break
    if not service_user_found:
        new_user = models.UserModel(
            username=SERVICE_USERNAME,
            password_hash=SERVICE_PASSWORD_HASH,
            restaurant_id=service_restaurant_id
        )
        new_user.add_to_db()


SERVICE_USERNAME = '*'
SERVICE_PASSWORD_HASH = '*'
SERVICE_RESTAURANT_NAME = 'service_restaurant'

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'

# app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+mysqlconnector://login:pass@localhost:8889/test"
# app.config["SQLALCHEMY_POOL_RECYCLE"] = 299

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = '*'
app.config['JWT_SECRET_KEY'] = '*'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = False

app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access']

db = SQLAlchemy(app)

JWT_ROLE_WAITER = 'waiter'
JWT_ROLE_CLIENT = 'client'


class JWTUserObject:
    def __init__(self, username, role):
        self.username = username
        self.role = role


jwt = JWTManager(app)

import views, models, resources


@jwt.user_claims_loader
def add_claims_to_access_token(user):
    return {'role': user.role}


@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.username


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return models.RevokedTokenModel.is_jti_blacklisted(jti)


db.create_all()
register_service_models()

# TODO: maybe allow waiters registration later
# TODO: token refresh and expire

# ######
# Service API
# TODO: может удалять пользователей и рестиков по айди?
# TODO: OPTIMIZE ALL SQL QUEIES
api.add_resource(resources.service.UserRegistration, '/api/service/user_registration')
api.add_resource(resources.service.RestaurantRegistration, '/api/service/restaurant_registration')
api.add_resource(resources.service.User, '/api/service/user')
api.add_resource(resources.service.Restaurant, '/api/service/restaurant')
api.add_resource(resources.service.RestaurantLicense, '/api/service/restaurant_license')
api.add_resource(resources.service.AllUsers, '/api/service/users')
api.add_resource(resources.service.AllRestaurants, '/api/service/restaurants')

# #######
# Waiter API
api.add_resource(resources.user.Login, '/api/user/login')
api.add_resource(resources.user.Logout, '/api/user/logout')
api.add_resource(resources.user.GenerateClientCode, '/api/user/generate_client_code')
api.add_resource(resources.user.EndClientSession, '/api/user/end_client_session')
api.add_resource(resources.user.GetNewMessages, '/api/user/get_new_messages')
api.add_resource(resources.user.GetActiveSessions, '/api/user/get_active_sessions')
api.add_resource(resources.user.GetActiveRegisteredSessions, '/api/user/get_active_registered_sessions')

# api.add_resource(resources.UserLogoutRefresh, '/logout/refresh')
# api.add_resource(resources.TokenRefresh, '/token/refresh')

# #########
# Client API
api.add_resource(resources.client.RegisterCode, '/api/client/register_code')
api.add_resource(resources.client.CallWaiter, '/api/client/call_waiter')
api.add_resource(resources.client.EndSession, '/api/client/end_session')

formatter = logging.Formatter(
    "[%(asctime)s] %(levelname)s - %(message)s")
handler = TimedRotatingFileHandler('app.log', when='midnight', backupCount=40)
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)
app.logger.addHandler(handler)

log = logging.getLogger('werkzeug')
log.setLevel(logging.DEBUG)
log.addHandler(handler)

limiter.logger.addHandler(handler)

if __name__ == '__main__':
    app.run()
