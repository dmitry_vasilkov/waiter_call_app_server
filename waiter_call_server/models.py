from run import db, SERVICE_RESTAURANT_NAME, SERVICE_USERNAME
from passlib.hash import pbkdf2_sha256 as sha256


class SaveToDbOrRollbackModel(db.Model):
    __abstract__ = True

    @staticmethod
    def safe_commit():
        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            raise e

    def add_to_db(self):
        db.session.add(self)
        self.safe_commit()

    def delete_from_db(self):
        db.session.delete(self)
        self.safe_commit()

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            cls.safe_commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}, 500

    @classmethod
    def get_all(cls):
        return cls.query.all()


class RestaurantModel(SaveToDbOrRollbackModel):
    __tablename__ = 'restaurants'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    is_license_active = db.Column(db.Boolean, default=False)
    users = db.relationship('UserModel', backref='restaurant', lazy=True)

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'id': x.id,
                'name': x.name,
                'is_license_active': x.is_license_active
            }
        return {
            'message': 'Success',
            'data': {'restaurants': list(map(lambda x: to_json(x), cls.query.all()))}
            }

    @classmethod
    def find_by_name(cls, name):
        return cls.query.filter_by(name=name).first()

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).filter(cls.name != SERVICE_RESTAURANT_NAME).delete()
            cls.safe_commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}, 500


class UserModel(SaveToDbOrRollbackModel):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password_hash = db.Column(db.String(120), nullable=False)
    restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurants.id"), nullable=False)
    sessions = db.relationship('SessionModel', backref='user', lazy=True)
    messages = db.relationship('MessageModel', backref='user', lazy=True)

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, stored_hash):
        return sha256.verify(password, stored_hash)

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'username': x.username,
                'password_hash': x.password_hash,
                'restaurant_id': x.restaurant_id
            }
        return {
            'message': 'Success',
            'data': {'users': list(map(lambda x: to_json(x), cls.query.all()))}
            }

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).filter(cls.username != SERVICE_USERNAME).delete()
            cls.safe_commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}, 500


class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti=jti).first()
        return bool(query)


class SessionModel(SaveToDbOrRollbackModel):
    __tablename__ = 'sessions'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    client_code = db.Column(db.Integer, nullable=False)
    table_number = db.Column(db.Integer, nullable=False)
    creation_date = db.Column(db.DateTime, nullable=False)
    registration_date = db.Column(db.DateTime, nullable=True)
    end_date = db.Column(db.DateTime, nullable=True)
    last_active_date = db.Column(db.DateTime, nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    is_registered = db.Column(db.Boolean, nullable=False, default=False)

    @classmethod
    def get_active_sessions(cls):
        return cls.query.filter_by(is_active=True)


class MessageModel(SaveToDbOrRollbackModel):
    __tablename__ = 'messages'

    id = db.Column(db.Integer, primary_key=True)
    message_content = db.Column(db.String(1000), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    client_code = db.Column(db.Integer, nullable=False)
    is_sent = db.Column(db.Boolean, nullable=False, default=False)
    creation_date = db.Column(db.DateTime, nullable=False)
    sent_date = db.Column(db.DateTime, nullable=True)
    table_number = db.Column(db.Integer, nullable=False)

    @classmethod
    def get_new_messages_for_user(cls, user):
        return cls.query.filter_by(user=user, is_sent=False)
