API: *

Управление сервером и бд: pythonanywhere.com *


Есть 3 разделения апи: Service API - имеем доступ только мы, для управления учетками; user api - для официантов; client api - соответственно для клиентов

Важно!
Для официантов мы должны сами зарегать аккаунты, для клиентов аккаунтов нет, они авторизуются по токену. Так же есть служебный аккаунт. Он одновременно является и служебным(права на создане удаление пользователей) и официантским
логин:*
пароль:*
Чтобы получить токен для него, необходимо залогиниться как и официант('/api/user/login')

усл обозначения:
1) AS(AS-T) - метод требует авторизации сервисного аккаунта c помощью логин пароля(с помощью токена)
2) AU(AU-T) - метод требует авторизации юзерского(официантского) аккаунта c помощью логин пароля(с помощью токена) 
3) AC-T - метод требует авторизации клиентского аккаунта с помощью токена

Для авторизации с помощью токена к запросу нужно добавить ХЕДЕР(не параметр!!!!):
Authorization: Bearer TOKEN


При успешном запросе сервис возвращает код 200 + message + возможно поле data c нужными тебе данными
При неудаче вернется только поле message с описанием ошибки, а так же код отличный от 200.
Как минимум, сервак можнт отправлять при неудаче следующие коды:
https://developer.mozilla.org/ru/docs/Web/HTTP/Status/400- плохой джсон либо недостаточно параметров
https://developer.mozilla.org/ru/docs/Web/HTTP/Status/429- слишком часто вызываешь апи
https://developer.mozilla.org/ru/docs/Web/HTTP/Status/401 - неправильный логин пароль, некорректный токен
https://developer.mozilla.org/ru/docs/Web/HTTP/Status/403- нет прав на действие(закончилась лицензия, обращаешься к апи официанта с токеном клиента и тд)
https://developer.mozilla.org/ru/docs/Web/HTTP/Status/500 - во всех остальных случаях
https://developer.mozilla.org/ru/docs/Web/HTTP/Status/405 - неправильный метод (POST GET
)
Пример неудачного ответа:
1)405 METHOD NOT ALLOWED
{
    "message": "The method is not allowed for the requested URL."
}

Методы типа GET нужны в основном для получения данных; методы POST нужны для отправки данных, методы DELETE нужны например для удаления юзеров и подобного.
перед каждым урлов написано либо что-то из усл обозначений либо тип метода

Теперь пойдет описание апи:
##########
# Service API
###########
AS-T POST '/api/service/user_registration' - регистрация нового ОФИЦИАНТА
params: username, password, restaurant_id(int)
answer:{
                'message': 'User {} was created'.format(data['username']),
                'data':{access_token': access_token}
                }



AS-T POST '/api/service/restaurant_registration' - регистрация рестика
params: name, is_license_active(bool)
answer:
{
                'message': 'Restaurant {} created'.format(data['name']),
                'data':{restaurant_id': new_restaurant.id}
                }




AS-T DELETE '/api/service/user' удаление официанта по имени
params:username
answer:             return {'message': 'Success'}



AS-T DELETE '/api/service/restaurant'
params: name
answer:
      return {'message': 'Success'}



AS-T POST '/api/service/restaurant_license' -   включение или отключение лицензии для рестика. при выключенной лицензии официант по идее не сможет регать клиентов новых
params: name, is_license_active(bool)
{
    "message": "Successfully set is_license_active to False"
}



AS-T GET '/api/service/users' - список всех юзеров
params: None
answer:
{
    "message": "Success",
    "data": {
        "users": [
            {
                "username": "super_account",
                "password_hash": "$pbkdf2-sha256$29000$sDZmDKHUmpOS8n5vLSWk1A$XUPB8TYpaVOCbCiu2D/CX65sEm4a7LPiEwtm.YuMqpk",
                "restaurant_id": 1
            }
        ]
    }
}

AS-T GET '/api/service/restaurants' - список всех рестиков
params: None
answer: 
{
    "message": "Success",
    "data": {
        "restaurants": [
            {
                "id": 1,
                "name": "service_restaurant",
                "is_license_active": false
            }
        ]
    }
}


AS-T DELETE '/api/service/users' - удаляет всех юзеров (за исключением сервисного)
params: None
answer:
{
    "message": "0 row(s) deleted"
}

AS-T DELETE '/api/service/restaurants' - удаляет всех рестиков (за исклчением сервисного)
params: None
answer: 
{
    "message": "0 row(s) deleted"
}




# ##################
# Waiter API
###############

AU/AS POST '/api/user/login'
params: username, password
answer:{
                    'message': 'Logged in as {}'.format(current_user.username),
                    'data':{access_token': access_token}
                    }


AU-T POST '/api/user/logout'
params: None
answer:{'message': 'Access token has been revoked'}


AU-T POST '/api/user/generate_client_code'
params:table_number(int)
answer: {
    "message": "Code successfully generated",
    "data": {
        "client_code": 9
    }
}


AU-T POST: '/api/user/end_client_session'
params: client_code(int)
answer: {'message': 'Success'}


AU-T GET '/api/user/get_new_messages' - получение новых сообщений. вроде как договорились что ты шлешь запрос каждые 5 сек. когда сообщения тебе отправятся, то в базе они становятся отправленными и в след раз тебе не придут
params: None
answer: 
{
    'message': 'Success',
    'data':         
        [
            {
                'message_content': new_message.message_content,
                'table_number': new_message.table_number,
                'creation_date': new_message.creation_date,
                'client_code': new_message.client_code
            },
            ...
        ]

}

AU-T GET '/api/user/get_active_sessions' - получение АКТИВНЫХ сессий, то есть сессий, для которых сгенерирован клиентский код, но он не обязательно зарегитрирован киентом
{
    "message": "success",
    "data":{
        "sessions": [
            {
                "user_id": 1,
                "creation_date": 1524601735,
                "registration_date": null,
                "last_active_date": null,
                "table_number": 1,
                "client_code": 8,
                "is_registered": false
            },
            {
                "user_id": 1,
                "creation_date": 1524601794,
                "registration_date": 1524601799,
                "last_active_date": null,
                "table_number": 1,
                "client_code": 0,
                "is_registered": true
            }
        ]
    }
}


AU-T GET '/api/user/get_active_registered_sessions' - получение АКТИВНЫХ ЗАРЕГИСТРИРОВАННЫХ сессий, то есть сессий, для которых сгенерирован клиентский код и этот код зарегистрирован клиентом
{
    "message": "success",
    "data": {
        "sessions": [
            {
                "user_id": 1,
                "creation_date": 1524601735,
                "registration_date": null,
                "last_active_date": null,
                "table_number": 1
            },
            {
                "user_id": 1,
                "creation_date": 1524601794,
                "registration_date": null,
                "last_active_date": null,
                "table_number": 1,
                "client_code": 0
            }
        ]
    }
}


# #################
# Client API
# ##############

POST '/api/client/register_code'
params: client_code
answer: 
                            'message': 'Code successfully registered',
                            'data':{access_token': access_token}


AC-T POST '/api/client/call_waiter' - тут пока что можно звать только своего официанта вроде как( или нет, советую проверить)
params: message_content
answer: {'message': 'Successfully sent'}


AC-T POST '/api/client/end_session' - это нужно после того, как ты вышел из рестика. как ты видел, завершить сессию может как официант, так и сам пользователь. Пока что я НЕ умею завершать сессию через 6 часов( как мы хотели раньше)
parms: None
answer:
{'message': 'Session finished successfully'}
